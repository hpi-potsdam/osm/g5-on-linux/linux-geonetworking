#define pr_fmt(fmt) KBUILD_MODNAME ": %s: " fmt, __func__
#include <linux/types.h>
#include <linux/bitfield.h>
#include <linux/hashtable.h>
#include <linux/spinlock.h>
#include <linux/skbuff.h>
#include <net/datalink.h>

#include <linux/gn_routing.h>
#include <linux/gn.h>

extern struct datalink_proto *gn_dl;

static DEFINE_HASHTABLE(gn_loc_t, 3);
static DEFINE_SPINLOCK(gn_loc_t_lock);

// ###### values in meters
#define EARTH_RADIUS 6378388ULL
#define PI 31415926ULL  // * 10^7
#define METER_PER_LON 111317ULL
#define RAD_PER_DEGREE 174533ULL  // * 10^7 - same as long and lat from PV

#define GN_LT_JIFFIES msecs_to_jiffies(GN_LOC_TE_LIFETIME)
#define GN_TST_VALID(tst) time_after(jiffies, (tst) + GN_LT_JIFFIES)

/**************************************************************************\
*                                                                          *
* GeoNetworking routing					   		   *
*                                                                          *
\**************************************************************************/

static u16 *gn_dpd_find(struct gn_dpd_buf *buf, u16 sn)
{
	int i = 0;
	for(; i < GN_DPL_SIZE; i++){
		if (buf->sn[i] == sn)
			return &buf->sn[i];
	}
	return NULL;
}

static void gn_dpd_insert(struct gn_dpd_buf *buf, u16 sn)
{
	buf->sn[buf->head] = sn;
	buf->head = (buf->head + 1) % GN_DPL_SIZE;
}

static u32 pdr(u32 old_pdr, u32 delta)
{
	if (!delta)
		delta = 1;
	return (GN_MAX_PDR_EMA_BETA * old_pdr) / 100 +
		(( (100 - GN_MAX_PDR_EMA_BETA) * (100 / delta) ) / 100);
}

// table is * 1000 | p1 * 100 entry
static int cos_table[] = {
  100000,  99995,  99980,  99955,  99920,  99875,  99820,  99755
,  99680,  99595,  99500,  99396,  99281,  99156,  99022,  98877,  98723,  98558
,  98384,  98200,  98007,  97803,  97590,  97367,  97134,  96891,  96639,  96377
,  96106,  95824,  95534,  95233,  94924,  94604,  94275,  93937,  93590,  93233
,  92866,  92491,  92106,  91712,  91309,  90897,  90475,  90045,  89605,  89157
,  88699,  88233,  87758,  87274,  86782,  86281,  85771,  85252,  84726,  84190
,  83646,  83094,  82534,  81965,  81388,  80803,  80210,  79608,  78999,  78382
,  77757,  77125,  76484,  75836,  75181,  74517,  73847,  73169,  72484,  71791
,  71091,  70385,  69671,  68950,  68222,  67488,  66746,  65998,  65244,  64483
,  63715,  62941,  62161,  61375,  60582,  59783,  58979,  58168,  57352,  56530
,  55702,  54869,  54030,  53186,  52337,  51482,  50622,  49757,  48887,  48012
,  47133,  46249,  45360,  44466,  43568,  42666,  41759,  40849,  39934,  39015
,  38092,  37166,  36236,  35302,  34365,  33424,  32480,  31532,  30582,  29628
,  28672,  27712,  26750,  25785,  24818,  23848,  22875,  21901,  20924,  19945
,  18964,  17981,  16997,  16010,  15023,  14033,  13042,  12050,  11057,  10063
,   9067,   8071,   7074,   6076,   5077,   4079,   3079,   2079,   1080,     0
,   -920,  -1920,  -2920,  -3919,  -4918,  -5917,  -6915,  -7912,  -8909,  -9904
, -10899, -11892, -12884, -13875, -14865, -15853, -16840, -17825, -18808, -19789
, -20768, -21745, -22720, -23693, -24663, -25631, -26596, -27559, -28519, -29476
, -30430, -31381, -32329, -33274, -34215, -35153, -36087, -37018, -37945, -38868
, -39788, -40703, -41615, -42522, -43425, -44323, -45218, -46107, -46992, -47873
, -48748, -49619, -50485, -51345, -52201, -53051, -53896, -54736, -55570, -56399
, -57221, -58039, -58850, -59656, -60455, -61249, -62036, -62817, -63592, -64361
, -65123, -65879, -66628, -67370, -68106, -68834, -69556, -70271, -70979, -71680
, -72374, -73060, -73739, -74411, -75075, -75732, -76382, -77023, -77657, -78283
, -78901, -79512, -80114, -80709, -81295, -81873, -82444, -83005, -83559, -84104
, -84641, -85169, -85689, -86200, -86703, -87197, -87682, -88158, -88626, -89085
, -89534, -89975, -90407, -90830, -91244, -91648, -92044, -92430, -92807, -93175
, -93533, -93883, -94222, -94553, -94873, -95185, -95486, -95779, -96061, -96334
, -96598, -96852, -97096, -97330, -97555, -97770, -97975, -98170, -98356, -98531
, -98697, -98853, -98999, -99135, -99262, -99378, -99484, -99581, -99667, -99744
, -99810, -99867, -99914, -99950, -99977, -99993,-100000};

/* icos() - look up in cos_table for a rad value.
 * @rad : the rad value.* 10^7
 *
 * Return : the cosine value * 100000
 */
static int icos(__s64 rad)
{
	if (rad > PI)
		rad = PI - (rad - PI);
	return cos_table[rad / 100000];
}

/* degree_to_rad() - convert a degree value to a rad value.
* @a : the degree value as 1/10 micro degree (10^7).
*
* Return : the rad value * 10^7.
*/
static __s64 degree_to_rad(__s64 a)
{
	return (((RAD_PER_DEGREE * a) / 10000000ULL ))  % (PI * 2ULL);
}

/* diff() - calculate the difference beween a and b.
* @a : value a.
* @b : value b.
*
* Return : the difference
*/
static __s32 diff(__s32 a, __s32 b)
{
	__s32 r = a - b;
	return r < 0 ? r * -1 : r;
}

/* get_distance() - calculate the distance of to position vectors
 * center/self: positionvectors, whose distance is calculated
 * @x : after execute includes the meter on X-axes.
 * @y : after execute includes the meter on Y-axes.
 *
 * the calculation based on pythagoras.
*/
static struct gn_coord gn_coord_diff(struct gn_coord lhs, struct gn_coord rhs)
{
	struct gn_coord c;
	s32 lat;
	lat = degree_to_rad((lhs.lat + rhs.lat) / 2);

	c.lat = (METER_PER_LON * icos(lat) * diff(rhs.lon, lhs.lon)) / (10000000ULL * 100000ULL);
	c.lon = (METER_PER_LON * diff(rhs.lat, lhs.lat)) / 10000000ULL;
	return c;
}

static inline struct gn_coord pv_to_coord(struct gn_lpv *pv)
{
	struct gn_coord c = {
		.lat = be32_to_cpu(pv->lat),
		.lon = be32_to_cpu(pv->lon),
	};
	return c;
}

static inline u64 dist(struct gn_coord c)
{
	return c.lat*c.lat + c.lon*c.lon;
}

/* gn_F() - decides if self is inside or at the border of the geographical area.
 * @center: The position vector of the Package sender.
 * @self: The own position vector.
 * @t : The Type of geographical area.
 * @r : The radius of area. Only use for circle.
 * @a : The width of area. Only use for rectangel and elipse.
 * @b : The heigth of area. Only use for rectangel and elipse.
 * @angel : ???
 *
 * Return: 	if the result is > 0 self is inside area.
			if the result is 0 self is on border of area.
			otherwise self is outside of area.
 */
__s64 gn_F(struct gn_coord self, struct gn_geo_scope scope)
{
	s32 a2, b2, x2, y2;
	s64 result = -1;
	struct gn_coord coord_diff = gn_coord_diff(self, scope.coord);

	// TODO Scope angle!
	a2 = scope.a * scope.a;
	b2 = scope.b * scope.b;
	x2 = coord_diff.lat * coord_diff.lat;
	y2 = coord_diff.lon * coord_diff.lon;

	switch (scope.shape) {
	case GN_SHAPE_CIRCLE:
		result = a2 - (x2 + y2);
		break;
	case GN_SHAPE_RECTANGLE:
		result = a2 - x2;
		if (result > (b2 - y2) ) {
			result = b2 - y2;
		}
		break;
	case GN_SHAPE_ELLIPSE:
		if (scope.a == 0 || scope.b == 0) {
			WARN_ONCE(1, "internal error: input out of range");
			break;
		}
		result =  1000;
		result -= (y2 * 1000) / a2;
		result -= (x2 * 1000) / b2;
		break;
	}

	return result;
}

static int greedy_forward(struct gn_iface *gnif, u8 *addr, struct gn_lpv *depv)
{
	struct loc_te *curr;
	int bkt, rc;
	u8 *found_addr = NULL;

	u64 min_dist, curr_dist, ego_dist;
	struct gn_coord dest_coord = pv_to_coord(depv);

	min_dist = ego_dist = dist(gn_coord_diff(dest_coord, gnif->pos.coord));
	spin_lock_bh(&gn_loc_t_lock);
	hash_for_each(gn_loc_t, bkt, curr, hnode) {
		if (curr->is_neighbour) {
			curr_dist = dist(gn_coord_diff(dest_coord, pv_to_coord(&curr->pv)));
			if (curr_dist < min_dist) {
				min_dist = curr_dist;
				found_addr = curr->ll_address;
			}
		}
	}

	// FIXME traffic class check here
	if (found_addr != NULL) {
		ether_addr_copy(addr, found_addr);
		rc = GN_FORWARD_NEXT_HOP;
	} else {
		rc = GN_FORWARD_BROADCAST;
	}
	spin_unlock_bh(&gn_loc_t_lock);

	return rc;
}

int gn_gxc_forward(struct gn_iface *gnif, s64 f, u8 *addr, struct gn_lpv *depv)
{
	if (f >= 0) {
		// Area forwarding
		switch (GN_AREA_FORWARDING) {
		case GN_AREA_FORWARDING_SIMPLE:
			return GN_FORWARD_BROADCAST;
		default:
			pr_warn("non-simple area forwarding not implemented");
			return GN_FORWARD_BROADCAST;
		}
	} else {
		// Non-area forwarding
		switch (GN_NON_AREA_FORWARDING) {
		case GN_NON_AREA_FORWARDING_GREEDY:
			return greedy_forward(gnif, addr, depv);
		default:
			return GN_FORWARD_BROADCAST;
		}
	}
}

static void debug_loc_te(void)
{
	struct loc_te *entry;
	int bucket;

	spin_lock_bh(&gn_loc_t_lock);
	if (hash_empty(gn_loc_t)) {
		spin_unlock_bh(&gn_loc_t_lock);
		return;
	}

	pr_info("Printing location table");
	hash_for_each(gn_loc_t, bucket, entry, hnode) {
		pr_info("LOC_TE(%p) tst=%x addr=%llx ll_addr=%llx is_neighbour=%x ls_pending=%x",
			entry, entry->tst_addr,
			be64_to_cpu(entry->addr), be64_to_cpu(entry->ll_address),
			entry->is_neighbour, entry->ls_pending);
	}
	spin_unlock_bh(&gn_loc_t_lock);
}

static void gn_prune(void)
{
	struct loc_te *entry;
	int bucket;

	spin_lock_bh(&gn_loc_t_lock);
	hash_for_each(gn_loc_t, bucket, entry, hnode) {
		if (GN_TST_VALID(entry->tst_addr)) {
			pr_info("pruning entry addr=%llx", entry->addr);
			skb_queue_purge(&entry->lsb);
			hash_del(&entry->hnode);
			kfree(entry);
		}
	}
	spin_unlock_bh(&gn_loc_t_lock);
}

/* update_location_table() - update location table
 * @pv: the position vector which indicate an entry.
 *
 * Return: 0 on success, 1 on error, 2 if packet is duplicate.
 *
 * Update an entry, which indicated by @spv. If no entry found its will be add a new one.
 * And all entries will be check with the update function.
 */
int gn_update_location_table(struct gn_lpv *pv, bool make_neighbour, const u8 *ll_address, const __be16 *sn)
{
	// FIXME Use timestamp associated with incoming skb
	// TODO (Clause C.2): Only update PV if incoming PV is newer than stored PV
	struct loc_te *entry;
	bool found = false;

	spin_lock_bh(&gn_loc_t_lock);
	hash_for_each_possible(gn_loc_t, entry, hnode, pv->addr) {
		if (entry->addr != pv->addr) {
			continue;
		}
		found = true;

		pr_info("updating entry addr=%llx", pv->addr);

		entry->pdr = pdr(entry->pdr, jiffies_to_msecs(jiffies) - jiffies_to_msecs(entry->tst_addr));
		entry->tst_addr = jiffies;
		entry->is_neighbour = entry->is_neighbour || make_neighbour;
		memcpy(&entry->pv, pv, sizeof(*pv));
		if (sn) {
			// Perform DPD
			if (gn_dpd_find(&entry->dpl, be16_to_cpu(*sn))) {
				pr_info("received duplicate packet");
				spin_unlock_bh(&gn_loc_t_lock);
				return 2;
			} else {
				gn_dpd_insert(&entry->dpl, be16_to_cpu(*sn));
			}
		}
		break;
	}

	if (!found) {
		entry = kzalloc(sizeof(struct loc_te), GFP_ATOMIC);
		if (!entry) {
			spin_unlock_bh(&gn_loc_t_lock);
			return 1;
		}
		pr_info("adding entry addr=%llx", pv->addr);
		entry->addr = pv->addr;
		entry->tst_addr = jiffies;
		entry->is_neighbour = make_neighbour;
		memcpy(&entry->pv, pv, sizeof(*pv));
		skb_queue_head_init(&entry->lsb);
		if (make_neighbour) {
			BUG_ON(!ll_address);
			ether_addr_copy(entry->ll_address, ll_address);
		}
		if (sn) {
			gn_dpd_insert(&entry->dpl, be16_to_cpu(*sn));
		}

		hash_add(gn_loc_t, &entry->hnode, entry->addr);
	}
	spin_unlock_bh(&gn_loc_t_lock);

	gn_prune();

	debug_loc_te();

	return 0;
}

static void __ls_queue(struct sk_buff_head *q, struct sk_buff *skb)
{
	struct sk_buff *curr;
	u32 qlen = skb_queue_len(q);
	while (qlen-- > GN_LSB_SIZE) {
		curr = skb_dequeue(q);
		if (curr)
			kfree_skb(curr);
	}

	skb_queue_tail(q, skb);
}

/* ls_queue() - queue packet for delivery if destination is not a neighbour
 * @dest_addr: destination address
 * @skb: the packet
 *
 * If the destination address is not a known neighbour ẃith recent activity,
 * queue the packet
 *
 * Return: GN_QUEUE_DIRECT if the destination is a neighbour, GN_QUEUE_LS_PENDING if the
 * destination is unknown, but a LS request is pending and the packet is queued,
 * GN_QUEUE_LS_STALE if the destination is unknown or its entry is stale and
 * we should send a LS query, GN_QUEUE_ERROR on error
 */
int gn_ls_queue(gn_address_t dest_addr, struct sk_buff *skb)
{
	struct loc_te *entry;
	int rc = -1;

	spin_lock_bh(&gn_loc_t_lock);
	hash_for_each_possible(gn_loc_t, entry, hnode, dest_addr) {
		if (entry->addr != dest_addr) {
			continue;
		}

		if (GN_TST_VALID(entry->tst_addr)) {
			// Entry is valid, no need for LS
			rc = GN_QUEUE_DIRECT;
		} else if (entry->ls_pending == 1) {
			// Entry is stale, but we already sent a LS request
			rc = GN_QUEUE_LS_PENDING;
		} else {
			// Entry is stale, perform LS request
			rc = GN_QUEUE_LS_STALE;

			entry->ls_pending = 1;
			__ls_queue(&entry->lsb, skb);
		}

		break;
	}
	if (rc == -1) {
		entry = kzalloc(sizeof(struct loc_te), GFP_ATOMIC);
		if (!entry) {
			spin_unlock_bh(&gn_loc_t_lock);
			return GN_QUEUE_ERROR;
		}

		rc = GN_QUEUE_LS_STALE;
		entry->addr = dest_addr;
		entry->ls_pending = 1;
		skb_queue_head_init(&entry->lsb);
		__ls_queue(&entry->lsb, skb);

		hash_add(gn_loc_t, &entry->hnode, entry->addr);
	}
	spin_unlock_bh(&gn_loc_t_lock);

	return rc;
}

void gn_ls_flush(gn_address_t dest_addr)
{
	struct loc_te *entry;
	struct sk_buff *tmp_skb;

	spin_lock_bh(&gn_loc_t_lock);
	hash_for_each_possible(gn_loc_t, entry, hnode, dest_addr) {
		if (entry->addr != dest_addr)
			continue;
		if (!entry->ls_pending)
			break;

		while ((tmp_skb = skb_dequeue(&entry->lsb)) != NULL) {
			// FIXME forwarding each packet according to its type is necessary
			// FIXME decrease packet lifetime according to time spent in queue
			gn_dl->request(gn_dl, tmp_skb, tmp_skb->dev->broadcast);
		}
		entry->ls_pending = 0;
		break;
	}
	spin_unlock_bh(&gn_loc_t_lock);
}

int gn_query_ll_address(gn_address_t query_addr, u8 *ll_address)
{
	struct loc_te *entry;
	int rc = 1;

	spin_lock_bh(&gn_loc_t_lock);
	hash_for_each_possible(gn_loc_t, entry, hnode, query_addr) {
		if (entry->addr != query_addr)
			continue;
		if (!entry->is_neighbour || !GN_TST_VALID(entry->tst_addr))
			break;
		if (is_zero_ether_addr(entry->ll_address))
			break;

		rc = 0;
		ether_addr_copy(ll_address, entry->ll_address);
		break;
	}
	spin_unlock_bh(&gn_loc_t_lock);

	return rc;
}
