/*
 * 	gn_proc.c - proc support for Appletalk
 *
 * 	Copyright(c) Arnaldo Carvalho de Melo <acme@conectiva.com.br>
 *
 *	This program is free software; you can redistribute it and/or modify it
 *	under the terms of the GNU General Public License as published by the
 *	Free Software Foundation, version 2.
 */

#include <linux/init.h>
#include <linux/proc_fs.h>
#include <linux/seq_file.h>
#include <net/net_namespace.h>
#include <net/sock.h>
#include <linux/gn.h>
#include <linux/export.h>

static void *gn_seq_socket_start(struct seq_file *seq, loff_t *pos)
	__acquires(gn_sockets_lock)
{
	read_lock_bh(&gn_sockets_lock);
	return seq_hlist_start_head(&gn_sockets, *pos);
}

static void *gn_seq_socket_next(struct seq_file *seq, void *v, loff_t *pos)
{
	return seq_hlist_next(v, &gn_sockets, pos);
}

static void gn_seq_socket_stop(struct seq_file *seq, void *v)
	__releases(gn_sockets_lock)
{
	read_unlock_bh(&gn_sockets_lock);
}

static int gn_seq_socket_show(struct seq_file *seq, void *v)
{
	struct sock *s;
	struct gn_sock *gn;

	if (v == SEQ_START_TOKEN) {
		seq_printf(seq, "Type Local_addr  Remote_addr Tx_queue "
				"Rx_queue St UID\n");
		goto out;
	}

	s = sk_entry(v);
	gn = gn_sk(s);

	seq_printf(seq, "%02X   %08llX:%04X  %08llX:%04X  %08X:%08X "
			"%02X %u\n",
		   s->sk_type,
		   be64_to_cpu(gn->src_addr), gn->src_port,
		   be64_to_cpu(gn->dst_addr), gn->dst_port,
		   sk_wmem_alloc_get(s),
		   sk_rmem_alloc_get(s),
		   s->sk_state,
		   from_kuid_munged(seq_user_ns(seq), sock_i_uid(s)));
out:
	return 0;
}

static const struct seq_operations gn_seq_socket_ops = {
	.start  = gn_seq_socket_start,
	.next   = gn_seq_socket_next,
	.stop   = gn_seq_socket_stop,
	.show   = gn_seq_socket_show,
};

int __init gn_proc_init(void)
{
	if (!proc_mkdir("gn", init_net.proc_net))
		return -ENOMEM;
	pr_debug("gn: inserting gn/socket into procfs");
	if (!proc_create_seq("gn/socket", 0444, init_net.proc_net,
			&gn_seq_socket_ops))
		goto out;

	return 0;
out:
	remove_proc_subtree("gn", init_net.proc_net);
	return -ENOMEM;
}

void gn_proc_exit(void)
{
	remove_proc_subtree("gn", init_net.proc_net);
}
